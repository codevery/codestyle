#Codestyle

## HTML 5

### Docktype

```html
<!DOCTYPE html>
```


### Self closing tags:

**Good**  space after tag name
```html
<br>
```
***Bad***
```html
<br/>
```


### Quotes

**Good** we are using double quotes for native html block
```html
<input type="text" name="email" disabled="disabled" />
```
***Bad***
```html
<input type='text' name='email' disabled='disabled' />
```
```html
<input type=text name=email disabled />
```


### Class
* Use global classes first
* Use local classes next
* Use frameworks \ libraries classes last

**Good**
```html
<div class="col-6 global local"></div>
```

* Use lowercase names
* Use **-** to separate names
* Use main (abstract) names first "head" is primary, "menu" is secondary

**Good**
```html
<div class="head-menu menu-green"></div>
```
***Bad***
```html
<div class="head_menu green-menu"></div>
```


### Id
* Use lowercase name
* Use underscore
* Use unique ID per page

**Good**
```html
<div id="music_action_play"></div>
```
***Bad***
```html
<div id="music_action_play"></div>
<div id="music_action_play"></div>

<div id="music-action-stop"></div>
<div id="musicActionStop"></div>
```






